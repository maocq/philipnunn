<?php 
/*
Plugin Name: Widgets PH Auto
Plugin URI: 
Description: Widgets content auto
Version: 1.0
Author: PhilipNunn
Author URI: 
License: GPL
Copyright: 
*/

function my_posts_where( $where ) {
	
	$where = str_replace("meta_key = 'widgets_%", "meta_key LIKE 'widgets_%", $where);
	return $where;
}

add_filter('posts_where', 'my_posts_where');


function get_widget_ph($from, $to) {
	$args = array(
	    'numberposts' => -1,
	    'meta_query' => array(
	    	'relation'		=> 'AND', 		    
	    	array(
	        	'key' => 'widgets',
	        	'value' => '0',
	        	'compare' => '>',
	      	),
			array(
	        	'key' => 'widgets_%_datew',
	        	'value' => $from,
	        	'compare' => '>=',
	      	),
	      	array(
	        	'key' => 'widgets_%_datew',
	        	'value' => $to,
	        	'compare' => '<=',
	      	)
		)
	);
	return new WP_Query( $args );
}

function shortcode_widget_ph_auto($atts) {

    $att = shortcode_atts( array(
        'lang' => '',
        'from' => '20000101',
        'to' => '20000101'
    ), $atts );   

    $langs = explode(',', $att['lang']);
    $from = $att['from'];
    $to = $att['to'];
    $widgets = get_widget_ph($from, $to);
	ob_start();
?>

<?php while ( $widgets->have_posts() ) : $widgets->the_post(); ?>

	<h3 class="title-w-auto"><a href="<?= get_the_permalink(); ?>"><?= get_the_title(); ?></a></h3>
	
	<?php if (have_rows('widgets')): ?>
		

		<?php while ( have_rows('widgets') ) : the_row(); ?>
				
			<?php 	
				$type = get_sub_field('type');
				$flag = get_sub_field('flag');
				$idiom = get_sub_field('idiom');
				$title = get_sub_field('title');
				$info = get_sub_field('info');
				$date = get_sub_field('datew');

				if (!in_array($idiom, $langs))
					continue;
				if ($date <= $from || $date >= $to)
					continue;					

			?>		


			<article class="widget-ph">
				<?php if((time()-(60*60*24*30)) < strtotime($date)): ?>
					<div class="new-w">NEW</div>
				<?php endif ?>
				<div class="content-w row-w">
					<div class="col-w-25">
						<div class="left">
							<figure>
								<img src="<?= $flag ?>">
							</figure>
						</div>
					</div>
					<div class="col-w-50">
						<div class="center">
							<h2><?= $title ?></h2>
							<div class="info-w">
								<?= $info ?>
							</div>
						</div>
					</div>
					<div class="col-w-25">
						<div class="right">
							<div class="type-w"><?= $type ?></div>
							<div class="icons-w">

								<?php if ($type === 'document'): ?>
									<?php if (get_sub_field('document')['mime_type'] === 'application/pdf'): ?>
										<a class="various-w fancybox.iframe" href="<?= get_sub_field('document')['url'] ?>">
											<span class="dashicons dashicons-visibility"></span>
										</a>										
									<?php endif ?>
									<a href="<?= get_sub_field('document')['url'] ?>" download>
										<span class="dashicons dashicons-migrate"></span>
									</a>
								<?php endif ?>

								<?php if ($type === 'audio'): ?>									
									<a class="audio-w fancybox.iframe" href="<?= get_sub_field('audio') ?>">
										<span class="dashicons dashicons-video-alt3"></span>
									</a>
									<?php $audioDownload =  get_sub_field('audio_download')?>
									<?php if ($audioDownload): ?>
										<a href="<?= $audioDownload['url'] ?>" download>
											<span class="dashicons dashicons-migrate"></span>
										</a>
									<?php endif ?>
								<?php endif ?>

								<?php if ($type === 'video'): ?>
									<a class="various-w fancybox.iframe" href="<?= get_sub_field('video') ?>">
										<span class="dashicons dashicons-video-alt3"></span>
									</a>
									<?php $videoDownload =  get_sub_field('video_download')?>
									<?php if ($videoDownload): ?>
										<a href="<?= $videoDownload['url'] ?>" download>
											<span class="dashicons dashicons-migrate"></span>
										</a>
									<?php endif ?>
								<?php endif ?>

							</div>
						</div>
					</div>
				</div>
			</article>
		

		<?php endwhile; ?>

	<?php endif ?>

<?php endwhile;
wp_reset_postdata();?>

<?php
	$result = ob_get_clean();
	return $result;
}


add_shortcode('WPP_AUTO', 'shortcode_widget_ph_auto');



//..//wp-admin/admin-ajax.php?action=auto_widget&from=20160901&to=20160930&title=September&key=3480a75931f9ba3f4beb292f510a093387c89741bc0705200ed0e00da868ea98
add_action('wp_ajax_nopriv_auto_widget', 'ajax_auto_widget');
add_action('wp_ajax_auto_widget', 'ajax_auto_widget');

function ajax_auto_widget(){

	$keyDefault = '3480a75931f9ba3f4beb292f510a093387c89741bc0705200ed0e00da868ea98';

	$key = $_REQUEST['key'];
	if ($key !== $keyDefault) exit;

	$from = $_REQUEST['from'];
	$to = $_REQUEST['to'];
	$title = $_REQUEST['title'];

	$content = '[:en][WPP_AUTO lang="en,es,fr" from="'.$from.'" to="'.$to.'"][:es][WPP_AUTO lang="es" from="'.$from.'" to="'.$to.'"][:fr][WPP_AUTO lang="fr" from="'.$from.'" to="'.$to.'"][:de][WPP_AUTO lang="de" from="'.$from.'" to="'.$to.'"][:it][WPP_AUTO lang="it" from="'.$from.'" to="'.$to.'"][:]';

	$my_post = array(
	'post_title'    => $title,
	'post_content'  => $content,
	'post_status'   => 'publish',
	);

	wp_insert_post( $my_post );

	$result = ['status' => 'ok'];
	header('Content-Type: application/json');
	echo json_encode( $result );
	exit;
}