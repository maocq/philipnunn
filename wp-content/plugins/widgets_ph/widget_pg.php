<?php 
/*
Plugin Name: Widgets PH
Plugin URI: 
Description: Widgets content
Version: 1.0
Author: PhilipNunn
Author URI: 
License: GPL
Copyright: 
*/


function shortcode_widget_ph($atts) {

    $att = shortcode_atts( array(
        'lang' => '',        
    ), $atts );   

    $langs = explode(',', $att['lang']);

	ob_start();
?>


	<?php if (have_rows('widgets')): ?>
		

		<?php while ( have_rows('widgets') ) : the_row(); ?>			
				
			<?php 	
				$type = get_sub_field('type');
				$flag = get_sub_field('flag');
				$idiom = get_sub_field('idiom');
				$title = get_sub_field('title');
				$info = get_sub_field('info');
				$date = get_sub_field('datew');

				if (!in_array($idiom, $langs))
					continue;

			?>		


			<article class="widget-ph">
				<?php if((time()-(60*60*24*30)) < strtotime($date)): ?>
					<div class="new-w">NEW</div>
				<?php endif ?>
				<div class="content-w row-w">
					<div class="col-w-25">
						<div class="left">
							<figure>
								<img src="<?= $flag ?>">
							</figure>
						</div>
					</div>
					<div class="col-w-50">
						<div class="center">
							<h2><?= $title ?></h2>
							<div class="info-w">
								<?= $info ?>
							</div>
						</div>
					</div>
					<div class="col-w-25">
						<div class="right">
							<div class="type-w"><?= $type ?></div>
							<div class="icons-w">

								<?php if ($type === 'document'): ?>
									<?php if (get_sub_field('document')['mime_type'] === 'application/pdf'): ?>
										<a class="various-w fancybox.iframe" href="<?= get_sub_field('document')['url'] ?>">
											<span class="dashicons dashicons-visibility"></span>
										</a>										
									<?php endif ?>
									<a href="<?= get_sub_field('document')['url'] ?>" download>
										<span class="dashicons dashicons-migrate"></span>
									</a>
								<?php endif ?>

								<?php if ($type === 'audio'): ?>									
									<a class="audio-w fancybox.iframe" href="<?= get_sub_field('audio') ?>">
										<span class="dashicons dashicons-video-alt3"></span>
									</a>
									<?php $audioDownload =  get_sub_field('audio_download')?>
									<?php if ($audioDownload): ?>
										<a href="<?= $audioDownload['url'] ?>" download>
											<span class="dashicons dashicons-migrate"></span>
										</a>
									<?php endif ?>
								<?php endif ?>

								<?php if ($type === 'video'): ?>
									<a class="various-w fancybox.iframe" href="<?= get_sub_field('video') ?>">
										<span class="dashicons dashicons-video-alt3"></span>
									</a>
									<?php $videoDownload =  get_sub_field('video_download')?>
									<?php if ($videoDownload): ?>
										<a href="<?= $videoDownload['url'] ?>" download>
											<span class="dashicons dashicons-migrate"></span>
										</a>
									<?php endif ?>
								<?php endif ?>

							</div>
						</div>
					</div>
				</div>
			</article>
		

		<?php endwhile; ?>

	<?php endif ?>


<?php
	$result = ob_get_clean();
	return $result;
}

wp_enqueue_style( 'widget_ph_css', plugins_url( 'css/widget_ph.css', __FILE__ ), array(), '1.1' );
wp_enqueue_script('widget_ph_js', plugins_url( 'js/widget_ph.js', __FILE__ ), array('jquery'));

wp_enqueue_style( 'fancybox_css', plugins_url( 'fancyBox/source/jquery.fancybox.css', __FILE__ ), array(), '1.0' );
wp_enqueue_script('fancybox_js', plugins_url( 'fancyBox/source/jquery.fancybox.js', __FILE__ ), array('jquery'));


add_shortcode('WPP', 'shortcode_widget_ph');

/********************************************************/
// Adding Dashicons in WordPress Front-end
/********************************************************/
add_action( 'wp_enqueue_scripts', 'load_dashicons_front_end' );
function load_dashicons_front_end() {
  wp_enqueue_style( 'dashicons' );
}